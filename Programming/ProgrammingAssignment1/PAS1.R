## rescribir GetDataFrame para que lea datos seguin ID
## adaptar corr() para que llame a GetDataFrame por ID, leyendo el ID del CSV
## corr() debe llamar a GetDataFrame(ID) si ese ID tiene NOBS>THRESSHOLD

## --------------------------------------------------------------------------------------------
corr<-function(DIRECTORY="specdata",THRESHOLD=0,PRINT=TRUE) {
  on.exit(expr={rm(list = ls())}, add = TRUE)
  ## Check if folder exists
  if (!dir.exists(DIRECTORY)) {   
    stop("invalid folder")
  } 
  
  COMPLETE <- complete(DIRECTORY,)
  ID<-COMPLETE[COMPLETE$nobs>THRESHOLD,"id"]
  ## NOBS<-COMPLETE[COMPLETE$nobs>THRESHOLD,"nobs"]
  ## Preallocate memory
  NR<-length(ID)
  CORR<-vector(mode = "numeric",length=NR)
  for (j in seq_along(ID) ) { ##
    ## Get Data Frame with both pollutants. (POLLUTANT=NULL)
    DF<-.GetDataFrame(ID = ID[j]) ## Data Frame with two columns
    # Clean data
    DF<-DF[complete.cases(DF),]
    CORR[j]<-cor(DF$nitrate,DF$sulfate)
  }
  ## DFC<-data.frame(id=ID,nobs=NOBS,corr=CORR)
  return(CORR)
}

## --------------------------------------------------------------------------------------------
complete<-function(DIRECTORY="specdata",SEQ=1:332) {
  on.exit(expr={rm(list = ls())}, add = TRUE)
  ## Check if folder exists
  if (!dir.exists(DIRECTORY)) {   
    stop("invalid folder")
  } 
  ## Preallocate memory
  NR<-length(seq_along(SEQ))
  NOBS<-vector(mode = "integer",length=NR)
  for (j in seq_along(SEQ)) {
    ## Get  file
    DATA<-.GetDataFrame(ID = SEQ[j],DIRECTORY) ## Dataframe with two columns
    ## Remove Null entries
    DATA<-DATA[complete.cases(DATA),]
    NOBS[j]<-sum(complete.cases(DATA))##nrow(DATA)
  }
  COMPLETE<-data.frame(id=SEQ,nobs=NOBS)
  return(COMPLETE)
}

## --------------------------------------------------------------------------------------------
pollutantmean<-function(DIRECTORY="specdata",POLLUTANT=NULL,SEQ=1:332) {
  on.exit(expr={rm(list = ls())}, add = TRUE)
  ## Check if folder exists
  if (!dir.exists(DIRECTORY)) {   
    stop("invalid folder")
  } 
  if (!(POLLUTANT %in% c("sulfate","nitrate"))) {
    error("invalid pollutant")
  }
  ## Preallocate memory
  NR<-length(seq_along(SEQ))
  MEAN<-vector(mode = "numeric",length=NR)
  NOBS<-vector(mode = "integer",length=NR)
  ## Get MEAN and NOBS for each CSV file
  j<-1
  #while (j <= NR) {
  for (j in seq_along(SEQ)) {
    DATA<-.GetDataFrame(ID = SEQ[j],DIRECTORY, POLLUTANT) ## Dataframe with one column
    ## Remove Null entries
    #DATA<-DATA[complete.cases(DATA)] ## Wrong. this subsetting method gives a Vector instead a df
    DATA<-subset(DATA,subset = complete.cases(DATA))
    
    if (nrow(DATA)>0) { ## Skip CSV files with all NA.
      ## Get MEAN and NOBS
      MEAN[j]<-mean(DATA[[POLLUTANT]])
      NOBS[j]<-as.integer(length(DATA[[POLLUTANT]]))
    } else {
      MEAN[j]<-0
      NOBS[j]<-0
    }
    #j<-j+1
  }
  # Get overall MEAN
  GMEAN<-(cumsum(MEAN*NOBS))[NR]/sum(NOBS)
  return(GMEAN)
}

## DF<-subset(subset = DF,!is.na(DF$nitrate),select = nitrate)
## DF<-subset(subset = DF,!is.na(DF$sulfate),select = sulfate)
## subset() is a convenience function intended for use interactively.
## For programming it is better to use the standard subsetting functions like [, and in particular 
## the non-standard evaluation of argument subset can have unanticipated consequences.

## --------------------------------------------------------------------------------------------
.GetDataFrame <- function (ID ,DIRECTORY="specdata",POLLUTANT=NULL){
  on.exit(expr={rm(list = ls())}, add = TRUE)
  ## Check directory
  if (!dir.exists(DIRECTORY)) {  
    stop("missing data folder")
  }  
  FILE<-.BuildFilename(DIRECTORY,ID)
  ## Check filename
  if (!file.exists(FILE)) {  
    stop("missing CSV file")
  }  
  ## Read CSV file
  DF<-read.csv(FILE,stringsAsFactors = FALSE) 
  ## Return a Data Frame
  if (!is.null(POLLUTANT)) { ## Case 1: get nitrate OR sulfate
    if ((POLLUTANT %in% colnames(DF))) {
      DATA<-subset(DF,select = POLLUTANT,drop = FALSE)
      #DATA<-subset(DF,complete.cases(DF),select = POLLUTANT,drop = FALSE)
    } else {
      stop("missing pollutant in CSV file")
    }
  } else {## Case 2: get nitrate and sulfate}
    DATA<-subset(DF,select = c("nitrate","sulfate"))
    #DATA<-subset(DF,complete.cases(DF),select = c("nitrate","sulfate"))
    ##DATA<-DF[,c("nitrate","sulfate")] ## class = data frame
  }
  return(DATA)
}

## --------------------------------------------------------------------------------------------
.BuildFilename <- function(DIRECTORY="specdata",ID){
  on.exit(expr={rm(list = ls())}, add = TRUE)
  ## Pad zeros in filename
  SID<-as.character(ID)
  NZP <- 3-nchar(SID,type = "chars")  ## length(SID) does not provide the string length
  ZP<-paste(rep("0",NZP),collapse = "")
  FILE<-paste(c("./",DIRECTORY,"/",ZP,SID,".csv"),collapse = "")
  return(FILE)
}
