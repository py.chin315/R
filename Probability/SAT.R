rm(list = ls())
questions<-44
choices<-5
# ====
# What is the probability of guessing correctly for one question?
p_correct<-1/choices
p_incorrect<-1-p_correct
score_correct<- +1
score_incorrect<- (-0.25)
# What is the expected value of points for guessing on one question?
score_correct*p_correct+score_incorrect*p_incorrect
# What is the expected score of guessing on all 44 questions?
m<-questions*(score_correct*p_correct+score_incorrect*p_incorrect)
# What is the standard error of guessing on all 44 questions?
s<-sqrt(questions)*abs(score_correct-score_incorrect)*sqrt(p_correct*p_incorrect)

# Use the Central Limit Theorem to determine the probability that a guessing student scores 8 points or higher on the test.
1-pnorm(8,m,s)
# Set the seed to 21, then run a Monte Carlo simulation of 10,000 students guessing on the test.

# ====
# (IMPORTANT! If you use R 3.6 or later, you will need to use the command set.seed(x, sample.kind = "Rounding") instead of set.seed(x). 
# What is the probability that a guessing student scores 8 points or higher?
B <- 10000
# set.seed(21, sample.kind = "Rounding")
set.seed(21, sample.kind = "Rounding")
p<-1/5
outcomes<-replicate(B,{
  X<-sample(c(-0.25,1), 44, replace = TRUE, prob = c(1-p, p))
  #X<-sample(c(0,1), 44, replace = FALSE)
  sum(X)
}
)
m<-mean(outcomes)
s<-sd(outcomes)
1-pnorm(8,m,s)
# ====
# Suppose that the number of multiple choice options is 4 and that there is no penalty for guessing - 
# that is, an incorrect question gives a score of 0.
# What is the probability of guessing correctly for one question?
rm(list = ls())
choices<-4
questions<-44
p_correct<-1/choices
p_incorrect<-1-p_correct
score_correct<- 1
score_incorrect<- 0
# What is the expected value of points for guessing on one question?
score_correct*p_correct+score_incorrect*p_incorrect
# What is the expected score of guessing on all 44 questions?
m<-44*(score_correct*p_correct+score_incorrect*p_incorrect)
# What is the standard error of guessing on all 44 questions?
s<-sqrt(44)*abs(score_correct-score_incorrect)*sqrt(p_correct*p_incorrect)
# ====
p <- seq(0.25, 0.95, 0.05)
prob<-function(p,score_correct=1,score_incorrect=0,questions=44) {
  
  m<-questions*(score_correct*p+score_incorrect*(1-p))
  s<-sqrt(questions)*abs(score_correct-score_incorrect)*sqrt(p*(1-p))
  1-pnorm(35,m,s)
}
sapply(p,prob)