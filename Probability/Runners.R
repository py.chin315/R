library(gtools)
library(tidyverse)
rm(list = ls())
runners <- c("Jamaica", "Jamaica", "Jamaica", "USA", "Ecuador", "Netherlands", "France", "South Africa")
medals<-c("Gold","Silver","Bronze")
expand.grid(medal=medals,runner=runners)
B<-10000
set.seed(1)
results<-replicate(B,{
  team<-sample(runners,3,replace=FALSE)
  all(team=="Jamaica")
})
mean(results)

