## Alejandro Verri Kozlowski. Civil Engineer

---
output: 
  html_document: 
    highlight: pygments
    keep_md: no
    smart: yes
    toc: yes
---
### Professional Record
#### Expertise
Alejandro is a Civil Engineer with +20 years of international experience in structural and geotechnical earthquake engineering and currently is the Practice Leader of the Civil and Mining Infrastructure Area of SRK Consulting Argentina. He finished his studies in 1997 at the University of Buenos Aires and submitted his thesis in 2003 entitled [*Non-linear numerical modeling of reinforced concrete structures subjected to cyclic loads*](https://www.researchgate.net/publication/265049854_Modelacion_Numerica_No-lineal_de_Estructuras_de_Hormigon_Armado_sometidas_a_Cargas_Ciclicas?_sg=WtBImTUBVj9mEOQUO9TyQVkhdxp5SDrYRnZCX86Spc0YcxaP_KLC2hyKha3s3nGYuQxXulBphOhA77dnVUS49Xwsb0wprc-XHV-XR4m-tcI.LBZX2XAh5ao2S7xHQ2V1q2ZAXfPvp-bpO0ogSWe2p_299sOfqCJkZDmsO1w0hv77UWJHOyuRI6aLD-lSDdkxYA). He has completed more than 25 post-graduate courses and has obtained 13 certifications in Seismic Engineering, Material Science, Geotechnical Earthquake Engineering and Data Science.

He worked as a structural consultant or reviewer in both mining and civil infrastructure projects, where he developed sound expertise in reliability-based analysis, seismic design and constructability analysis of underground structures and mining infrastructure, from pre-feasibility stages to closure and post-closure stages. Before joining SRK, he was a consultant and director of Verri & Sfriso SA (V&SSA), a consulting firm specialized in soil-structure interaction problems, where he participated until its merger with SRK in 2009, in several projects of structural design of special foundations, pavements, tunnels and underground works.  He has also participated in several major environmental rehabilitation projects and mine closure studies, where he prepared seismic and hazard stability studies for tailings dams, dumps, and leach pads, drafted the seismic design criteria and project execution plans (PEP), and prepared risk-based cost estimates. In addition, he was the design leader and the Engineer of Records for the closure and environmental rehabilitation of the Pascua-Lama Tunnel on the border between Argentina and Chile. He has developed extensive field experience in reliability-based seismic analysis and design of tailing and rockfill dams, waste rock dumps, tunnels, retaining walls, buried pipelines and infrastructure works, and has conducted site response studies and probabilistic seismic hazard assessments at project sites in Argentina, Chile, Peru, Costa Rica, Honduras, Mexico, the United States, Canada, Armenia, South Africa, Madagascar, New Zealand and Australia.

Alejandro has been a professor of the Composite Materials Mechanics chair at the School of Engineering of the University of Buenos Aires from 2004 to 2014. He was also an assistant professor in the Structural Dynamics courses of the master’s degree in Structural Engineering at the National Technological University (UTN), and a visiting professor in the Structural Analysis courses of the Postgraduate Program in Architecture at the Torcuato Di Tella University.
Alejandro has published 16 research articles, of which he has authored 8 conference papers, has delivered eleven lectures at international conferences and workshops and has attended 20 international congresses. He has been an enthusiastic computer programmer since the days of the Algol and Fortran languages and has developed several open-source tools for [*performance-based earthquake engineering*](https://gitlab.com/PBEE) 

#### Specialization
**Geotechnical Earthquake Engineering**: Probabilistic and Deterministic Seismic Hazard Assessments; Dynamic Site Response Analysis; Maximum Design Earthquakes (MDE, MCE) and Seismic Record Selection for dynamic (time-history) analysis of rockfill das and Tailing Storage Facilities; Blast-Induced and Seismic-Induced Deformation Analysis; Seismic Coefficient for Performance-based Stability Analysis of Slopes and Embankments.

**Civil & Structural Engineering**: Reliability-based Analysis and Design. Numerical modeling and Simulation for reliability-based analysis of tunnel linings, shafts and underground structures; Structural and Foundation Engineering; Pre-feasibility and feasibility Engineering Studies for Mining Infrastructure; Constructability Analysis and Probabilistic-based cost estimate for construction and demolition works; Forensic engineering and root-cause analysis of failures in buried pipelines, tunnels, and underground structures.

#### Employment Record
* 2009 - 2020. Practice Leader and Director of SRK Consulting Argentina S.A.
* 2000 - 2009: Consultant Engineer and Partner of V&S SA until its merger with SRK.
* 1997 - 2000: Freelance Civil Structural Designer

#### Professional Registrations
* Professional Council of Civil Engineering - Buenos Aires (CPIC) Registration #15584
* Professional Council of Engineering and Surveying - San Juan (CPIA). MP #4212

#### Affiliations
* American Society of Civil Engineers (ASCE). Associate #379469.
* Earthquake Engineering Research Institute (EERI). Associate # 12953
* Seismological Society of America (SSA) 
* Argentinian Committee of Dams (CAP) 
* International Commission of Large Dams (ICOLD)
* Chilean Association of Seismology and Earthquake Engineering (ACHISINA). Associate #094 
* Association of Structural Engineers (AIE) Fellow member #347 

#### Languages
* Spanish (native)
* Portuguese (Fluent)
* English (Fluent)

#### Programming Languages & Frameworks
* Fluent with Wolfram (Mathematica), R (RStudio), MATLAB, C, FORTRAN
* Familiar with sed, Perl, AWK, FORTH, git, UNIX bash/shell

### Education
#### University Degree
* Diploma in Civil Engineering, University of Buenos Aires (1991-1997). Undergraduate Thesis (submitted in 2003) 

#### Postgraduate Courses
* (2019) “Geotechnical earthquake engineering”. Instructors: Johnatan Bray y Steve Kramer. Classroom course at ACHISINA , Santiago, Chile. July 25,26,27 de julio. 
* (2019) “Data Science: Productivity Tools” by Harvard University at EDX. e-learning Course 
* (2019) “Data Science: R Basics” by Harvard University at EDX. e-learning Course 
* (2029) “Data Science: R Programming” by Johns Hopkins University on Coursera. e-Learning Course 
* (2019) “Data Science: Introduction to the Tidyverse” by DataCamp. e-Learning Course 
* (2019) “Data Science: Probability” by Harvard University at EDX. e-learning Course 
* (2016) “Risk Assessment and Decision Making for Mine Geowaste Facility Management” online webcast by EduMine. Instructors: P. Littlejohn, F. Oboni, J. Caldwell. June 7,8,9 
* (2016) “Introduccion al Análisis y Evaluación del Riesgo en la Ingeniería de Presas” Seminar at the (CAP), Cipolletti, Argentina. Instructor. F. Giuliani. Sep/20.
* (2014) “Seismic Design of Tailings Dams.” Instructors: J. Meneses, L. Oldecop, F. Zaballa. Seminar at UNSJ, San Juan, Argentina. April 28/29
* (2013) “Research Reports Writing in English”. Postgraduate Course. G. Martín. 40 hs.
* (2009) “Displacement-based seismic design”, Instructors: Priestley, R. & Calvi, M. Seminar at UNC, Mendoza, Argentina Sep/3,4 
* (2009)  “Failure indicators in continuous media”. Postgraduate Course. G. Etse. 40 hs. 
* (2008) “Numerical modeling of cohesive-frictional materials”. Postgraduate Course. G. Etse. 
* (2008) “Continuum Mechanics”. Postgraduate Course.  E. Dvorkin FIUBA. 80 hs.
* (2005) “Rockfill Dams.”, Instructor: Eduardo Núñez. Seminar at Techint Buenos Aires, Argentina
* (2005) “Performance-based seismic design”. Instructors:R. Bertero. Seminar at Techint Buenos Aires, Argentina 
* (2000) “Seismic design of retaining walls”. Instructor: Raúl Bertero. Seminar at AIE, Buenos Aires, Argentina. Jun/7 to Jul/20
* (2000) “Numerical and experimental simulation of concrete cracking”. Postgraduate Course. A. Alvaredo. 60 hs 
* (1996) “Introduction to Linear Fracture Mechanics” Instructor: José Rusell. Seminar at AIE, Buenos Aires, Argentina. July/11 to July/18. 


### Academic Record
#### Employment Record
* 2004-2014, Full Professor and Assistant Professor. Civil Engineering. University of Buenos Aires (FIUBA)
* 2009, External Professor. Postgraduate School of Architecture, Torcuato Di Tella University (UTDT)
* 2008, Assistant Professor. Master in Structural Engineering. National Technological University (UTN). 
* 1996-2004, Teaching Assistant. Civil Engineering School. University of Buenos Aires (FIUBA)

#### Regular Courses Delivered
* (2004 a 2013) Composite Materials Mechanics. Full Professor. (FIUBA)  
* (2008) Structural Dynamics. External Professor. Master in Structural Engineering, (UTN)
* (2007) Structural Analysis II-A. Assistant Professor of Prof. R. Carretero. (FIUBA)
* (2007) Structures II. External Professor. (UTDT)
* (2009, 2012) Seismic Analysis.  Assistant Professor of Dr. Bertero. (FIUBA)
* (2005,2008). Structural Dynamics II Assistant Professor of Dr. Bertero. (FIUBA)
* (2007,2012).  Reliability. Assistant Professor of Dr. Bertero. (FIUBA). 
* (2004,2006). Structural Dynamics I. Assistant Professor of Dr. Bertero. (FIUBA)
* (2003-2004). Structural Analysis IV. Teaching Assistant of Dr. R. Bertero (FIUBA). 
* (1997-1998).  Numerical Methods IA. Teaching Assistant of Prof. G. Gonzalez. (FIUBA)
* (1996). Foundations Engineering. Teaching Assistant of Prof. E. Núñez (FIUBA). 


#### Other Academic Activities
* (2012) Jury for the selection of candidates for teaching assistants for the Soil Mechanics chair of the Department of Stability (FIUBA)  
* (2012) Jury for the selection of candidates for teaching assistants for the Materials Science chair of the Department of Stability (FIUBA)  
* (2009) Jury for the selection of candidates for Professors for the chair of Ship Structures at the Department of Naval Engineering of the Faculty of Engineering (FIUBA)
* (2008) Jury for the selection of candidates for Professors for the chair of Mechanical Vibrations of the Department of Mechanical Engineering (FIUBA)



### Researh & Development Record
#### Publications
* Verri A., F. Mussat, J. Garate, (to be submitted in 2020) *Reliability-based seismic design of hydraulic plugs in deep mining tunnels.* XVIII International Probabilistic Workshop 2020. Minho, Portugal.
* Verri, A., Levis D, Mussat J (To be submitted in 2020). *Supervised learning models for prediction of seismic-induced displacements in Tailing Storage Facilities*, XVIII International Probabilistic Workshop 2020. Minho, Portugal.
* Ledesma O, Verri A, Sfriso A (2018). Numerical modeling of a rockfill tailings dam subject to seismic loading. IX V CAP (Mendoza, Argentina).
* Ledesma O, Verri A, Sfriso A (2017). Deformation analysis of an AFRD for mine tailings storage and power generation. XIX ICSMGE (Seúl, Corea).
* Lehmann, A. Verri, A.; Bertero, A: Muñoz, S. (2012) [*Consideraciones de diseño y construcción de una mesa vibradora para ejecución de ensayos dinámicos*](https://www.researchgate.net/publication/337952058_CONSIDERACIONES_DE_DISENO_Y_CONSTRUCCION_DE_UNA_MESA_VIBRADORA_PARA_EJECUCION_DE_ENSAYOS_DINAMICOS?_sg=4JtfCeTj2eaCSJmESeXcJW2UyreUdBBvNxI99hv-X-w1QR6iAcDh-49DL5l_OU9PQ4_fPcmOgg0-slVXrLOA2erFmzWnbGCCbhmWfCSx.ib1mv8pf1XmXqDlApmm4o2CD_O_8pg9OL_NZaBLix9rs9ljS8mKu-3snccRjWek0INys-LXMjk1VIPeWrD87Fg). XXII Jornadas Argentinas de Ingeniería Estructural (Buenos Aires, Argentina).
* Bertero, R., Mussat, J., Verri, A. (2012) [*Vibraciones en Edificios originadas por la hinca de Tablestacas en Centros Urbanos*](https://www.researchgate.net/publication/337952053_VIBRACIONES_EN_EDIFICIOS_ORIGINADAS_POR_LA_HINCA_DE_TABLESTACAS_EN_CENTROS_URBANOS). XXII Jornadas Argentinas de Ingeniería Estructural (Buenos Aires, Argentina).
* Bertero, R.; Lehmann, A.; Verri, A.; Mussat, J. & Vaquero, S. (2010) [*Vibraciones en edificios cercanos originadas por espectáculos musicales en estadios*](https://www.researchgate.net/publication/337951968_VIBRACIONES_EN_EDIFICIOS_CERCANOS_ORIGINADAS_POR_ESPECTACULOS_MUSICALES_EN_ESTADIOS), XXI Jornadas Argentinas de Ingeniería Estructural (Buenos Aires, Argentina).
* Bertero, R.; Verri, A. & Lehmann, A. (2009) Criterios para construir el espectro de diseño elástico y los sismos de diseño para un sitio dado, VIII EIPAC (Mendoza, Argentina).
* Bertero, R.; Verri, A. & Lehmann, A. (2008) *Coeficientes de seguridad para el diseño sísmico basado en la performance*, XX Jornadas Argentinas de Ingeniería Estructural (Buenos Aires, Argentina). 
* Verri, A. (2007) [*Análisis no-lineal de vigas anisótropas de laminados compuestos según una teoría modificada de deformaciones por corte*](https://www.researchgate.net/publication/265049867_ANALISIS_NO-LINEAL_DE_VIGAS_ANISOTROPAS_DE_LAMINADOS_COMPUESTOS_SEGUN_UNA_TEORIA_MODIFICADA_DE_DEFORMACIONES_POR_CORTE), ENIEF 2007 (Córdoba, Argentina).
* Verri, A. (2006) [*Un modelo numérico simple para el análisis no-lineal de vigas prismáticas de laminados compuestos*](https://www.researchgate.net/publication/228704595_UN_MODELO_NUMERICO_SIMPLE_PARA_EL_ANALISIS_NO-LINEAL_DE_VIGAS_PRISMATICAS_DE_LAMINADOS_COMPUESTOS), Congreso de Métodos Numéricos en Ingeniería (Porto, Portugal).
* Sfriso, A. & Verri, A. (2004) [*Ensayo de fricción de la interfaz de un pavimento postensado*](https://www.researchgate.net/publication/242617979_ENSAYO_DE_FRICCION_DE_LA_INTERFASE_BASE_-_PAQUETE_ESTRUCTURAL_DE_UN_PAVIMENTO_POSTESADO), XVII CAMSIG (Córdoba, Argentina). 
* Verri, A. & Del Vecchio, A. (2001) [*Tablestacados con múltiples niveles de atensoramiento*](https://www.researchgate.net/publication/337951899_TABLESTACADOS_CON_MULTIPLES_NIVELES_DE_ATENSORAMIENTO), II Congreso Argentino de Ingeniería Portuaria (Buenos Aires, Argentina). 
* Verri, A. (2011) [*Selección de registros para el análisis y diseño sísmico de presas de enrocado*](https://www.researchgate.net/publication/280600380_Seleccion_de_registros_para_el_analisis_y_diseno_sismico_de_presas_de_enrocado). Anales de la Academia Nacional de Ingeniería. Tomo VI, año 2010. Buenos Aires, Argentina
* Verri, A. (2005) [*Funciones de forma clase C1 considerando deformaciones por corte y torsión para elementos de viga 3-D de laminados compuestos*](https://www.researchgate.net/publication/337951987_FUNCIONES_DE_FORMA_CLASE_C1_EN_ELEMENTOS_DE_VIGA_CONSIDERANDO_DEFORMACIONES_POR_CORTE_Y_TORSION), Monografías en Mecánica de Laminados Compuestos FIUBA 

#### Attendance at Conferences 
* IX CAPyAH. Argentine Congress of Dams and Hydraulic Uses. Mendoza, Argentina (2018)
* Georisk-2017 ASCE Conference. Denver, US. (2017)
* XI Chilean Congress of Seismology and Earthquake Engineering ACHISINA 2015. Santiago de Chile  2015)
* I International Seminar on Mining Waste and Heap Leach Facilities. Lima, Perú. (2017)
* II Peruvian Seminar on Geoengineering. Sociedad Peruana de Geo-ingeniería (SPG). Lima, Perú. (2016)
* XXXII PERUMIN International Mining Convention, Arequipa, Perú (2015)
* I CDME International Congress of Mine Design by Empirical Methods. Lima. Perú. (2014)
* X NCEE National Conference on Earthquake Engineering, Anchorage, Alaska, USA (2014)
* II IPBD. International Conference on Performance-Based Design on Earthquake Geotechnical Engineering. Taormina, Italia. (2012)
* II International Symposium on Rockfill Dams, Río de Janeiro, Brazil. (2011)
* V ICEGE International Conference on Earthquake Geotechnical Engineering, Santiago, Chile. (2011)
* VIII EIPAC Seismic Resistant Design Seminar. Mendoza, Argentina. (2009)
* XX JAIE Argentine Conference on Structural Engineering, Buenos Aires, Argentina. (2008)
* XVI ENIEF Congress on Numerical Methods and their Applications, Córdoba, Argentina. (2007)
* III Luso-Brazilian Congress of Geotechnics, Curitiba, Brasil. (2006)
* I WSUS International Symposium on Waterproofing Underground Structures, São Paulo, Brasil (2005)
* II Argentine Congress of Port Engineering. Buenos Aires, Argentina. (2001)
* I Argentine Congress of Port Engineering. Buenos Aires, Argentina. (1999)
* WCCM World Congress of Computational Mechanics. Buenos Aires, Argentina (1998)


#### Lectures delivered
* Verri, A. [*Selection of Design Earthquakes for Dynamic Analysis of Tailings Dams*](https://www.researchgate.net/publication/337952007_Seleccion_de_los_Sismos_de_Diseno_para_el_Analisis_Dinamico_de_Presas_de_Relaves) Conferencia dictada en el I Seminario Internacional de Residuos Mineros y Pilas de Lixiviación. Lima, Perú. Noviembre de 2017
* Verri, A. [*Performance Based Seismic Design (PBSD) of Rockfill Dams*](https://www.researchgate.net/publication/337952088_Diseno_sismico_basado_en_la_Performance_PBSD_de_Presas_de_Enrocado)*.* Conferencia Magistral dictada en II Seminario Peruano de Geoingeniería. Sociedad Peruana de Geoingeniería (SPG). Lima, Perú. 4 al 6 de mayo de 2016
* Verri, A. [*Performance-based seismic design of mine tunnel plugs*](https://www.researchgate.net/publication/337952005_Diseno_sismico_basado_en_la_performance_de_tapon_de_obturacion_de_tunel_minero) Conferencia dictada en II Seminario Peruano de Geoingeniería. Sociedad Peruana de Geoingeniería (SPG). 4 al 6 de mayo de 2016
* Verri A. [*Seismic Design Considerations for Tailing Dams*](https://www.researchgate.net/publication/337952081_Consideraciones_sobre_Diseno_Sismico_de_Presas_de_Relaves). XXXII Conferencia Magistral dictada en la Convención Minera PERUMIN, Arequipa, Perú. Septiembre 2015
* Verri A. [*Reliability-based design of Tailing Dams*](https://www.researchgate.net/publication/337952008_Reliability-based_design_of_Tailing_Dams) SRK Tailings. Workshop Reno, USA. Sept. 2018
* Verri A. “Performance-Based Seismic Analysis of Dams, Slopes, Walls and Embankments” SRK Technical Presentation. Vancouver, Canada. Julio 2014
* Verri A. “Seismic Record Selection & Scaling Issues in Nonlinear Soil-Structure Interaction Problems” SRK Internal Technical Presentation. Anchorage, Alaska. Julio 2014
* Verri A., “*Towards a Common Approach in Nonlinear Seismic Analy*sis” SRK Workshop on Numerical Modelling Salta, Argentina. Sept. 2012
* Verri A. Marchetti, L. Garate F. y Barbieri P. “*Performance Based Seismic Design of a Mining Plug*”, Jornadas del Departamento de Estabilidad, FIUBA, Buenos Aires, Argentina. Septiembre 2016
* Verri A., *Uncertainties in Seismic Design of CFRD Dams*, Jornadas del Departamento de Estabilidad, FIUBA, Buenos Aires, Argentina. 29 y 30 de septiembre 2015
* Verri A. *Performance Based Seismic Design on CFRD Dams*, Jornadas del Departamento de Estabilidad, FIUBA. Buenos Aires, Argentina. Agosto 2014

#### R&D Projects
* Namazu (2013-) Open-Source libraries for performance-based earthquake engineering in R, Wolfram and Matlab: Newmark Sliding Block Displacement Analysis (NDA), Blasting-induced deformation analysis (BIDA), Dynamic Site Response Analysis (DSRA), Probabilistic Seismic Hazard Assessment (PSHA), Deterministic Seismic Hazard Assessment (DSHA), Reliability-based Seismic Design of Tunnel Plugs (PLUG), Ground-Motion Signal Processing Tools (GMSP), Ground-Motion Records Database (GMDB), Seismic-induced Displacement Analysis in Pipelines (PIPE), Finite-Element Analysis of RC Sections (FEARCS)
* PICT (2010) Design of a shaking table for seismic testing of scale models. Structural Dynamics Lab. FIUBA 
* PICT (2009) Analysis of Rayleigh waves in stratified media generated by spectators during concerts. Structural Dynamics Lab. FIUBA 



### Professional Experience
#### Key Experience: Geotechnical Earthquake Engineering. (2005-2019)
* (2019) [*Tronox Everglades RSF*](https://www.srk.co.za/en/za-tronox-everglades-rsf). KwaZulu Natal, South Africa. Consultant Engineer. Prepared seismic hazard estimate for a series of tailings dams. The work included the selection of the maximum design earthquakes and the definition of the horizontal seismic coefficient for the stability analysis of the tailings dam walls during construction, closure and post-closure. For SRK Africa
* (2019)  Solomon Hub Tailings Dam. Pibara, Australia. Consultant Engineer. Prepared a study of blast induced residual deformation in the vicinity of a tailings dam. The work included the development of numerical models and open-source code for the estimation of peak velocity attenuation at different blast sequences. For SRK Australasia
2019 Geghanush Dam, Armenia. Consultant Engineer. Performed seismic hazard assessment and selection of seismic record for dynamic analysis of a Tailings Storage Facility. For SRK UK
* (2019) Mareesburg Tailings Dam. Limpopo, South Africa. Consulting Engineer. Prepared seismic risk assessment and seismic design selection for dynamic analysis of a tailings dam. For SRK Africa
* (2019) Mandena Mine. Fort Dauphin, Madagascar. Consulting Engineer. Developed the site seismic hazard estimate and the definition of the horizontal seismic coefficient for the stability analysis of 70 m high slopes for the construction, operation and closure stages of the water storage facilities. For SRK Africa
* (2019) Copper City Mine Open Pit Arizona, USA. Consulting Engineer. Reviewer of seismic hazard studies for mine closure. Work included definition of maximum design seismic and selection of seismic records for dynamic analysis of embankments and mine roads around the pit. For SRK Vancouver/BHP
* (2018) Pascua-Lama Mine. Coquimbo, Chile. Consultant Engineer. Developed the analysis of alternatives for the removal and reprofiling of the main waste dump. The scope of the study included the review and update of the site's seismic hazard studies, the analysis of the seismic stability of the slopes and banks, and the design of slopes and berms with minimum risk of rockfall Developed the seismic stability analysis and rockfall analysis for the design of the main dump removal works. For Compañía Minera Nevada
* (2018) Los Bronces Mine, Chile. Consulting Engineer. Participated as a reviewer of the seismic hazard studies of the Perez Caldera Dam Closure Plan. For Fluor/Anglo-American
* (2018) Cerro Vanguardia Mine. Santa Cruz, Argentina. Seismic consultant and reviewer of seismic hazard studies for tailings dam reclamation plan. Definition of the Maximum Credible Earthquake for the Closure and Post-closure stage. For AngloGold Ashanti.
* (2017) Veladero Mine. San Juan, Argentina. Consulting Engineer. Participated in the project team of the mine closure plan in conceptual and pre-feasibility stages. Work included the analysis of the seismic stability of the leach pad and dumps, the definition of the seismic design criteria for slopes and embankments. For Barrick Minera Argentina Sol
* (2017) [*HB Mine Tailings Facility*](https://rdck.ca/EN/main/services/waste-recycling/hb-mines-tailings-facility.html). Salmo, BC, Canada. Consulting Engineer. Developed seismic hazard studies for a tailings dam in post-closure condition. Work included definition of maximum design earthquake and selection of seismic records for dynamic analysis. For SRK Canada
* (2016) [*Frieda River Project*](https://goo.gl/maps/C7xP5cXM8YRwrGiU9). Papua New Guinea. Consultant Engineer. Elaborated the selection of design earthquakes for the dynamic analysis of a 170 m high rockfill dam. Work included Site Response analysis and processing of seismic records for PLAXIS. For SRK Australia
* (2016) Pascua Lama Mine, Argentina-Chile. Consultant Engineer. Prepared the seismic hazard assessment to estimate the maximum design pressures of the hydraulic seal of the Pascua-Lama binational tunnel. For Barrick Exploraciones Argentinas
* (2015) [*Mina Salar del Rincón*](https://goo.gl/maps/MA9p6Lrf3EER8YZh9). Salta. Consultant Engineer. Prepared the seismic hazard assessment of a Lithium process plant. The work included the definition of the seismic design criteria and the definition of the horizontal seismic coefficient for slopes and embankments. For ADY Resources.
* (2015) [*Minto Mine.*](https://goo.gl/maps/XfLrcML7pJSskx6i8) Yukon, Canada. Consultant Engineer. Prepared the selection of design seismic for dynamic analysis of a tailings dam. Work also included dynamic site response analysis and estimation of expected permanent settlements on the dam crest. For SRK Canada
* (2015) [*Mina Justa*](https://goo.gl/maps/gK3DSXMXPQTizY876), Perú́. Consultant Engineer. Prepared the probabilistic seismic hazard studies of a high-altitude copper mine. The work included the definition of the seismic coefficient for the pit stability analysis and the definition of the Seismic Design Criteria for the Feasibility engineering. For SRK Peru/Marcobre
* (2014) [*Rabbit Lake*](https://en.wikipedia.org/wiki/Rabbit_Lake_mine) and [*Key Lake*](https://en.wikipedia.org/wiki/Key_Lake_mine) Tailings Dams. Saskatchewan, Canada. Consulting Engineer. Developed the probabilistic seismic hazard studies for the project site. Work included definition of uniform hazard (UHS) acceleration spectra and definition of hard ground accelerations. For SRK Canada/Cameco Corp.
* (2014) [*Mina Aranzazu*](https://goo.gl/maps/yhbBjJBEmgBMFgmt8). Zacatecas, Mexico. Consulting Engineer. Developed the probabilistic seismic hazard studies of the project site. The work included the definition of the uniform hazard (UHS) acceleration spectra, the definition of the horizontal seismic coefficient and the selection of the maximum design earthquakes for the deformation analysis of an earth dam. For SRK Canada
* (2013) [*Kitimat LNG Facility*](https://goo.gl/maps/upcqywcdEiDPcAqk9). Alberta, Canada. Consulting Engineer. Reviewer of the seismic design criteria of the MSE wall, and prepared an estimate of the horizontal seismic coefficient for performance-based limit equilibrium of a mechanically stabilized earth (MSE) wall.
* (2012) [*Terminal de Embarque de Minerales El Callao*](https://goo.gl/maps/ghqh6hVdT3fwS5HR6). Lima, Peru. Consultant Engineer. Participated as a peer reviewer of seismic design criteria for dock and port structures. The work included the elaboration of the design criteria of the pier for the bidding and construction. For Larrague & Associates.
* (2012) [*Proyecto Hidroeléctrico Reventazón*](https://goo.gl/maps/VKZ5Lo8jhw1uJdU16). Limón, Costa Rica. Consultant Engineer. Prepared a probabilistic analysis of expected permanent deformations of a concrete-faced rockfill dam (CFRD) The work included the selection of design earthquakes for the dynamic analysis in FLAC and the review of the seismic hazard studies of the site. For Costa Rican Institute of Electricity (ICE) through SRK Chile
* (2010) [*Puerto Cortés*](https://goo.gl/maps/xdoTP7REePdy12yGA), Honduras. Consultant Engineer. Peer Reviewer. Elaborated the technical recommendations for the seismic design of the pier at the new terminal of the seismic design criteria of pier and port structures. The work included the definition of the maximum acceptable levels of damage in the different components and structures, the minimum criteria of ductility of inelastic response of the piles and the recommended analysis methodologies for the numerical models. For World Development Bank (BID)
* (2009) [*Presa Puntilla del Viento*](http://www.smi-chile.cl/proyectos/diseno-embalse-puntilla-del-viento.html), Los Andes, Chile. Consultant Engineer. Prepared the selection of seismic design records for the dynamic analysis of a 140 m concrete screen rockfill dam. The work also included the review of site seismic hazard studies, dynamic site response analysis and processing of seismic records. For SMI Engineers through SRK Chile.
* (2009) [*Parque Eólico El Totoral*](https://goo.gl/maps/GgyXgkHC4bz4YX7j9). Canela, Chile. Consultant Engineer. Developed an estimation of the horizontal seismic thrust coefficient and the definition of seismic design criteria for slopes and embankments in a wind farm. For SRK Chile.
* (2009) Breakwater. Bahía Ancón. Perú́. Consultant Engineer. Participated in the safety analysis of the breakwater failure and settlement due to seismic action. For Juan Larrague and Associates
* (2005) Ancoa Dam. Linares, Chile. Seismic Consultant. Participated in the 3D dynamic analysis of a rockfill dam with a concrete screen of 160 m height. The work included the estimation of permanent settlements and the site response analysis. For SMI Ingenieros through SRK Chile

#### Key Experience: Mining Infrastructure
* (2019) Veladero Mine. San Juan, Argentina. Consultant Engineer. Coordinator of the team that developed the mine closure plan in the conceptual and pre-feasibility stages. The work included the cost estimate for the high-altitude gold heap leach operation, the updating of CapEx and OpEx, and the probabilistic analysis of contingency costs. For Barrick Minera Argentina Sol
* (2018) Pascua Lama Project. Coquimbo, Chile. Consultant Engineer. Coordinator of the team that prepared the scoping studies, pre-feasibility and feasibility closure plans and cost estimates for a partially constructed high altitude gold mine. For Compañía Minera Nevada
* (2017) Pascua Lama Project, San Juan, Argentina. He was the Engineer of Records before the Government of San Juan for the construction of a hydraulic mining plug of high environmental risk, in the Pascua-Lama binational tunnel. The work included the preparation of the Project Execution and the risk-based cost estimate of construction works. In 2015, he was also the Leader of the geotechnical, seismic and structural design team. For Barrick Exploraciones Argentinas.
* (2013) Pascua Lama Project, La Higuera, Chile. Consultant Engineer. Responsible for the design of special platforms and structures to transport equipment through ore bin pass shafts. Work included the design of special steel frames to support and hoist 150-ton platforms and the design of special rockfall protection covers. For Nevada Mining Company
* (2013) Pascua Lama Project, La Higuera, Chile. Consultant Engineer. Developed the constructability analysis of the Pascua underground works. The work included the redesign of existing feeder equipment supports, belts, monorails and bridge cranes in the Lower Primary Crushing Cavern. For CMN Compañía Minera Nevada
* (2013) Pascua Lama Project, La Higuera, Chile. Consultant Engineer. Prepared the design of the lining of the 70 m high ore bin pass under the primary crushing cavern. The work included the technical-economic analysis of alternatives and the design of abrasion resistant concretes for CMN Compañía Minera Nevada
* (2013) Dominga Mining and Port Project. La Higuera, Chile. Pre-feasibility engineering team leader for an Iron Concentrate Mine facility. The work included the design of the roof structures of large spans, the design of equipment foundations, industrial pavements, channels and chambers, pools and emergency tanks, slope and grade stability analysis and the design of superstructure and foundations of a 2 km long conveyor belt. For SRK Chile/Andes Iron.
* (2012) Pascua Lama Project, La Higuera, Chile. Project team leader for the conceptual design and detail engineering of the Lower Primary Crushing Cavern (ore-bin pass) plugs. For Compañía Minera Nevada
* (2012) Pascua Lama Project, Chile. Consultant Engineer. Responsible for the constructability analysis of an underground crusher box. The work included the revision of the original structural design, the revision of the seismic design criteria and the optimization of the geotechnical design of the lateral support system of the crusher building. For Compañía Minera Nevada
* (2011) [*Yacimiento Carbonífero Rio Turbio*](https://goo.gl/maps/9vVZcbLfCbNs5vdo9) (YCRT). Santa Cruz, Argentina. Consulting Engineer. Led the team of the Constructability analysis and structural optimization of the mine facilities. The work included the review of seismic design criteria for all facilities, the design of large span pavements for the stockpile beaches, the seismic redesign of the shrinkage armatures for a 2 m thick foundation plate and the optimization of the conveyor belt foundation system. For the Isolux-Corsán consortium.
* (2011) [*Mina Pirquitas*](https://goo.gl/maps/2RdB8yrnwUzPc4wA9).. Palpalá, Jujuy. Consultant Engineer. Project team leader for the design of hydraulic structures for surface management of contact and non-contact water in a silver mine. The work included the design of surface pipelines, diversion chambers, sedimentation chambers, and slope protection works. For Silver Standard
* (2007) Los Pelambres Mine. Salamanca, Chile. Consultant Engineer. Project team leader for the rehabilitation of the lining of a diversion tunnel. The work included the design of crack control reinforcement and optimization of the existing anchorage system in the radial slab. For Antofagasta Minerals
* (2005) [*Túnel Las Animas Camino La Pólvora*](https://goo.gl/maps/3ht3mwQTpaNyaicFA), Valparaiso, Chile. Consultant Engineer responsible for the executive design of the false ceiling slab for a 2.4 km tunnel For SRK Chile
* (2010) Potasio Rio Colorado (PRC) Project. Bahía Blanca, Argentina. Structural project team leader for a process plant, mat foundations, pavements, port facilities and underground structures. For CH2M/Vale PRC

#### Key Experience: Engineering Risk Analysis. Forensic Engineering
* (2020) Ezulwini Plugs, South Africa Consultant Engineer. Prepared the back-analysis studies for the estimation of the ultimate failure load of a series of hydraulic plugs in deep mining tunnels, for an extreme seismic event. The work included the development of advanced FEM and proxy models for reliability-based seismic analysis. For SRK South Africa
* (2019) Los Patos Aqueduct, Catamarca, Argentina. Consultant Engineer. Prepared the Seismic Hazard studies of the layout and estimation of the failure probability of a 40 km long buried pipeline. For Livent S.A.
* (2018) SAM Hydroelectric Power Plant, Lima, Peru Risk analysis of an 11 km long adduction tunnel. The work included the forensic analysis of old damages and the interpretation of new images and data obtained by using sonar from a remotely operated vehicle (ROV), the estimation of the lining collapse load and the risk analysis of support failure due to hydraulic fracture. For ElectroPerú and Black&Veatch
* (2018) Pascua Lama Project, Argentina-Chile. Prepared the Failure Risk Analysis and the quality control and quality assurance (QA/QC) systems for a high environmental risk hydraulic plugging work in the Pascua-Lama binational tunnel. The work included the probabilistic failure analysis, the elaboration of the environmental contingency plans and the risk management plan. For Barrick Exploraciones Argentinas SA
* (2017) La Confluencia Hydroelectric Power Plant, La Higuera, Chile Failure Risk Analysis and Root Cause Analysis of a 4 km long adduction tunnel. The work included the development of advanced FE models for the numerical simulation of concrete lining cracking, and the dent buckling failure of a steel lining due to water hammer produced by the sudden closure of valves for Black & Veatch
* (2010) Arroyo Maldonado Tunnel. Buenos Aires, Argentina. Consultant. Participated with the client's Risk Analysis and Management team and prepared a risk exposure analysis of the project during the construction of the tunnel. For Leza, Escriña y Asociados
* (2008) North Sewerage Connector. Buenos Aires, Argentina. Consultant Engineer. Led the team of forensic analysis of instability produced in a large diameter HDPE sewer. The work included the formulation and implementation of advanced numerical FEM models to investigate the interaction of the viscoelastic effect and the equilibrium instability (dent buckling) of a submerged pipeline in a very low density backfill. For CARBE S.A. 
* (2009) [*Central Termoeléctrica Andina*](https://www.saidi.es/PDF/Project_Profiles/Project_profile_MEJILLONES.pdf). Mejillones, Chile. Consultant Engineer. Prepared the seismic hazard and failure probability studies of a large diameter GRP pipe buried for reactor cooling. For Amitech/Foster Wheeler
* (2008) Mina Los Pelambres, Salamanca, Chile. Prepared a conceptual study of risk of collapse and rockfall due to an extreme seismic event in a 6 km diversion tunnel (Quillayes tunnel). The work included the conceptual analysis of seismic hazard, the review of the design of the Quillayes tunnel support system, the design of a rockfall protection system and the presentation of the findings and results to the mining authority (SerNaGeoMin). For Antofagasta Minerals through SRK Chile.

#### Key Experience: Civil Infrastructure 
* (2010) Underground Line E, Buenos Aires, Argentina. Consultant Engineer responsible for the design and detail engineering of the connecting cavern of four tunnels and the underground access ramp. For Benito Roggio e Hijos
* (2010) Underground Line A, Buenos Aires, Argentina. Consultant Engineer. Participated in the structural design team of 4 km of tunnels in the Primera Junta - Nazca leg. The work included the design and structural verification of the Nazca Workshop Garage and the Plaza Flores Power Substation. For Dycasa.
* (2010) SIEPAC project(. Consultant Engineer. Led the design team and structural rehabilitation of foundations for four power plants of the electrical interconnection system of Costa Rica, Nicaragua, Honduras and Guatemala. For Techint Ingeniería y Construcción
* (2010) Rio de La Plata Terminal Pier. Buenos Aires, Argentina. Consultant Engineer responsible for the design and detail engineering of the 25 meters high concrete curtain walls. For Soletanche-Bachy/TRP
* (2009) [*Exolgán Container Terminal*](https://goo.gl/maps/DoJmGzZKxpiFKn6Z8). Buenos Aires, Argentina. Consultant Engineer responsible for the structural design of 30 m high anchor walls, anchorage systems, rail beams and mooring structure in the access channel to the terminal for Morro Cuatro Bocas, Morro Oeste and Morro P.N.A. for Larrague y Asociados
* (2008) Underground Line B, Buenos Aires. Consultant Engineer. Participated in the structural design team of 2 km of tunnels in the section Los Incas - Villa Urquiza and Echeverría and Roosevelt stations. For Benito Roggio e Hijos
* (2008) Foundation Engineering. Foundation plates on piles, anchored walls and earth retaining structures for underground parking lots: Saint Regis Hotel, City of Buenos Aires; Alvear Hotel. City of Buenos Aires;[*Complejo Fórum Puerto Norte*](https://goo.gl/maps/b6CuHh8JhLvxoFUj6). Rosario; *Complejo Shopping Al Rio*. Vicente López, Buenos Aires
* (2006)  Underground Line B, Buenos Aires, Argentina. Consultant Engineer. Participated in the structural design team of the Lacroze - Los Incas section and Tronador and Los Incas stations. For Techint - Dywidag consortium
* (2005) Underground Line 4. Santiago, Chile. Consultant Engineer. Review and optimization of construction procedures for tunnels and shafts in the Plaza Egaña metro station. The work included the design and verification of the primary lining of the shaft for the asymmetric state of loads due to lateral excavation and the transitory state of a post-tensioned anchorage system. For the OHL consortium 
* (2005) Metro Line 4. Santiago, Chile. Consultant Engineer. Participated in the optimization of construction procedures for tunnels and shafts at Puente Alto station. For th1 VEI consortium 
* (2005) Blasting Rooms. San Luis, Entre Rios & Buenos Aires. Consultant Engineer. Designed a series of special reinforced concrete and steel structures for the relief of deflagration pressures of butane propellants in five industrial plants For Canon Puntana/Palmolive.
* (2004) Underground Line 4. Santiago, Chile. Consultant Engineer. Review and optimization engineering for tunnels and shafts at Los Orientales metro station. The work included the design of a mixed structure to replace the central portal of the tunnel. For the FAB consortium through SRK Santiago.
* (2004) Underground Line 4. Santiago, Chile. Consultant Engineer. Review and optimization engineering for of an access shaft and early works for the Tobalaba metro station. For the CSP consortium
* (2003) Once Underground Metro Station Buenos Aires, Argentina. Consultant Engineer responsible for the structural design of the tunnels and stations in Cavern of the Venezuela - Corrientes segment. The work included the structural engineering of an underground metro station. For Dycasa
* (2001) [*São João and Tigrinho Viaducts*](https://mapio.net/pic/p-22657884/). Parana, Brazil. Consulting Engineer. Participated in the structural design of the reinforcement and structural rehabilitation of slabs of a two-span curved bridge for a design load change

#### Key Experience: Structural Engineering (1998-2001)
* Schools, Hospitals and Public Buildings [*Hospital Dr. Máspero*](https://goo.gl/maps/TsZ45cCrzSxYbFGu9), Remedios de Escalada, Buenos Aires; [*Clínica ALPI*](https://goo.gl/maps/wErgCxLu1dsrPBo4A), Soler 2945, Buenos Aires City; [*Escuela de Comercio N°17*](https://goo.gl/maps/Y7pyFLjz7wEh45dEA), Buenos Aires City; [*Hospital de Rehabilitación Manuel Rocca*](https://goo.gl/maps/H8g4jYB5hQhCaaQ59), Buenos Aires City; [*Escuela EPET N°9*](https://goo.gl/maps/Wzb2pjhf7y7zgbgP7), Alem, Misiones;* [*Instituto Universitario Naval*](https://goo.gl/maps/QuKRbouNmhX4HKqd8), Vicente López, Buenos Aires;
* Industrial buildings, large span roofs, pre-cast concrete industrial buildings. Rehabilitation of structures and foundations for load changes [*Bolsaplast Plant*](https://goo.gl/maps/cFEMU4vznsBRADbD7) , Alem, Misiones; General Mills Plant. Burzaco, Buenos Aires; Temis-Lostaló Laboratory, Buenos Aires City; EDS Telecommunications Node, Buenos Aires City; ; [*Complejo El Divino Buenos Aires*](https://images.app.goo.gl/w9rccn2rHCJjDMxH6), City of Buenos Aires. [*Railway Station FFCC Gral. Mitre*](https://goo.gl/maps/hUwgMrgjERF9uQwe8). Buenos Aires City.
* Buildings and Housing Developments. 10 story building in [*Corcuera 31*](https://goo.gl/maps/xvCXQJqAS22SPszy5), Remedios de Escalada, Buenos Aires; 14 story building in [*Azcuénaga 1968*](https://goo.gl/maps/V61ezAwjK3yHQvC38), Buenos Aires City; 12 story building in [*Pacheco de Melo 2525*](https://goo.gl/maps/FRn3kKZsrk9KBW267), Buenos Aires City; 13 story building in [*Aristóbulo del Valle 175*](https://goo.gl/maps/pdSwnjBKBQrXAz7N6), Lanús, Buenos Aires; 12 story building in [*Ministro Brin 2736*](https://goo.gl/maps/dgc9ZUpSyZUPpqRv7), Lanús, Buenos Aires; 8 story building in [*Ministro Brin 2901*](https://goo.gl/maps/tNJFNrDgWHKCNpEV9), Lanús, Buenos Aires City; 12 story building in [*Fray Justo S.M de Oro 2671*](https://goo.gl/maps/AUHNprdb5VFV2PmT6), Buenos Aires City; 13-story building at [*Boatti 345*](https://goo.gl/maps/8qQnH1MXg6VZfhVT7), Morón, Buenos Aires; 10-story building at [*Brown 2120*](https://goo.gl/maps/Xm6CiCLtdndMSc4y7), Lomas de Zamora, Buenos Aires; 15-story building at [*Las Heras 1840*](https://goo.gl/maps/kULUiyo3sURVpME58), City of Buenos Aires; 11-story building at [*Ayacucho 1232*](https://goo.gl/maps/sfmzet7csdLp9m1eA), Buenos Aires City; 15-story building at [*Lavalle 1441*](https://goo.gl/maps/Lgjs3XvhPmYbzepD6), Buenos Aires City; [*Suarez 479 and 502 Housing Complex*](https://goo.gl/maps/H2eg2smKX3FsCPdA6), Buenos Aires City; 14-storey building in [*Thames 676*](https://goo.gl/maps/Tmwu7cJ6uStRjPgp8), Buenos Aires City; [*Naval University Institute*](https://goo.gl/maps/QuKRbouNmhX4HKqd8), Vicente López, Buenos Aires; 13-storey building in Pacheco 2725, Buenos Aires City; 15-storey building in [*García del Rio 2693*](https://goo.gl/maps/kuJ2jNiNahZ9u5iPA), Buenos Aires City
* Small bridges, docks, sheet piles and minor works of coastal protection for parks and public walks: [*Paseo del Viento*](https://goo.gl/maps/cYkCtvERLWtnGBmi6). Vicente López, Argentina; [*Parque de La Memoria*](https://goo.gl/maps/RyQ97r5VE5XdfFUG8), Buenos Aires, Argentina; [*Parque de Los Niños*](https://goo.gl/maps/RsEpzqgXD4awJE3FA). Buenos Aires, Argentina